import React, { Component } from 'react';
import './App.css';
import Button from './components/Button';
import ListItem from './components/ListItem';
import makeid from './utils/generateId';

class App extends Component {

  constructor () {
    super();
    this.state = {
      todos: [],
      user: {},
      loginState: false,
      valueTodo: ''
    }

    this.handleChange = this.handleChange.bind(this);
  }

  buttonAtas = () => {
    alert('ini button dari atas')
  }

  buttonBawah = () => {
    alert('ini button dari bawa')
  }

  // showEmpty = () => {
  //   if(this.state.loginState) {
  //     return <div>Kamu belum menambahkan todo</div>;
  //   } else {
  //     return <div>Kamu belum menambahkan todo, login aja</div>
  //   }
  // }

  handleChange(event) {
    let value = event.target.value;
    this.setState({valueTodo: value})
    // this.setState({valueTodo: event.target.value});
  }

  handleSubmit(value) {
    if(value.length > 3) {

      let todos = this.state.todos;
      todos.push({
            todo: value,
            id: makeid(10)
      })
      this.setState({
        todos: todos,
      })
    } else {
      alert('please input more than 5 characters')
    }
  }

  render() {
    const { todos, valueTodo } = this.state;
    console.log('todos', todos);
     return (
      <div className="App">
        <header className="App-header">
          <p>Todo App</p>
          <label>Todo</label>
          <input
            onChange={this.handleChange}
            type="text"
            id="fname"
            name="todo"
            placeholder="Your todo.."
          />
          <Button title="Add Todo" onClick={() => this.handleSubmit(valueTodo)}/>
          {valueTodo ? (
            <div>Your input: {valueTodo}</div>
          ) : <div></div>}
          <ul>
            {todos.length ? todos.map((todo) => (
              <ListItem key={todo.id} content={todo.todo} />
            )) : (<div>Kamu belum menambahkan todo</div>)}
          </ul>
        </header>
      </div>
  );
  }
}

export default App;
