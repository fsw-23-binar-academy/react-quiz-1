
// stateless component
const ButtonHello  = (props) => {
  const { title, className, onClick } = props;
  return (
    <div
        onClick={onClick ? onClick : () => alert('No action provided')}
        className={className ? className : 'button-style-primary'}
    >
      {title ? title : 'Button'}
    </div>
  )
}

export default ButtonHello;